#!/usr/bin/env python
# -*- coding: cp1252 -*-
# utf-8 latin-1 iso-8859-1 cp1252

"""
Convertir un fichier XL vers CSV
"""

from __future__ import unicode_literals
from __future__ import print_function

# import openpyxl
#import pandas
#import xlrd

__authors__      = "Barack Obama"
__date__         = "2023"
__version__      = "0.0.0"
__status__       = "DEV" # PROD



# https://stackoverflow.com/questions/1523427/what-is-the-common-header-format-of-python-files
# https://peps.python.org/pep-0263/
# https://medium.com/@rukavina.andrei/how-to-write-a-python-script-header-51d3cec13731
# https://fr.wikipedia.org/wiki/Windows-1252
# TODO https://peps.python.org/pep-0350/

# https://stackoverflow.com/questions/59071483/how-to-set-encoding-as-ansi-using-python
# https://stackoverflow.com/questions/15502619/correctly-reading-text-from-windows-1252cp1252-file-in-python

#print("� fichier 1252 mais ligne coding utf-8")
# SyntaxError: (unicode error) 'utf-8' codec can't decode byte 0xe9 in position 0: invalid continuation byte

#print("� fichier 1252 mais ligne coding windows1252")
# SyntaxError: encoding problem: windows1252

print("� fichier 1252 avec # -*- coding: cp1252 -*-  ca fonctionne !")

# TODO Faire les tests de codepage sous Unix
# winscp est parametre pour transfert en BINAIRE.
##  $ python ./bin/xlExport.py
##  � fichier 1252 avec # -*- coding: cp1252 -*-  ca fonctionne !
##  $ file ./bin/xlExport.py
##  ./bin/xlExport.py: Python script, ISO-8859 text executable, with CRLF line terminators


# FIXME
# BUG
