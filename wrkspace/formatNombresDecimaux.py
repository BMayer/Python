## Comment formater un nombre decimal ?
# respect du signe, nbr zeros devant, nbr max de decimales.

# https://docs.python.org/fr/3/library/functions.html#format
# https://docs.python.org/fr/3/library/string.html#formatspec
# https://docs.python.org/fr/3/library/string.html#format-specification-mini-language
# https://stackoverflow.com/questions/46645374/python-format-with-leading-zeros-and-sign

print("*** nombre <-- -1.1 ***")
nombre = float(-1.1)
#print("%-03.5f", "%-03.5f".format(nombre))
print("%02d" % (nombre,), "%02d --> -1")

print("%03d" % (nombre,), "%03d --> -01")

print("%04d" % (nombre,), "%04d --> -001")

print("%.2f" % (nombre,), "%.2f --> -1.10")

print("%.6f" % (nombre,), "%.6f --> -1.100000")

print(type("%.6f" % nombre), "%.6f --> str")

print("%000.2f" % (nombre,), "%000.2f --> -1.10")

print("{:.2f}".format(nombre), "{:.2f} --> -1.10")

print("{:.6f}".format(nombre), "{:.6f} --> -1.100000")

print("{:0.6f}".format(nombre), "{:0.6f} --> -1.100000")

print("{:000.6f}".format(nombre), "{:000.6f} --> -1.100000")

print("{:03.6f}".format(nombre), "{:03.6f} --> -1.100000")

print("{value:+0{width}.{precision}f}".format(value=nombre, width=12, precision=6), " --> -0001.100000")

print("{:000.2f}".format(nombre), "%000.2f ou bien {:000.2f}--> -1.10")