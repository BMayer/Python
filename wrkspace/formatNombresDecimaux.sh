#!/usr/bin/bash

if [[ ${USER} == 'bernard' ]]; then
    pythonRun="python3"
    homeDir="/home/bernard/repoGit/Bateau/TaC/NMEA_utils"
else
    pythonRun="/c/Logiciels/Anaconda3/python"
    homeDir="/c/AncienDisqueD/RepoS/Bateau/TaC/NMEA_utils"
fi

${pythonRun} ${homeDir}/nombresDecimaux.py #${homeDir}/../Traces/echantillonTest.nmea
